# improved version based on mimesis, WIP

import pandas as pd
from mimesis.schema import Fieldset, Field, Schema
from mimesis.locales import Locale


field = Field(locale=Locale.EN_GB)
schema = Schema(
    schema=lambda: {
        "id": field("increment"),
        "uid": field("uuid"),
        "name": field("person.full_name"),
        "email": field("person.email")
    },
    iterations=5
)
frame = pd.DataFrame(schema.create())

