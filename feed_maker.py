import pandas as pd
from faker import Faker
import logging
import csv
import os
import json
import sys
from datetime import datetime


class Options:

    def __init__(self, max_len=None, elements=None, ref_dict=False):
        self.max_len = max_len
        self.elements = elements
        self.ref_dict = ref_dict


class Column:

    def __init__(self, name, column_type: str, options: Options = None):
        self.name = name
        self.type = column_type
        self.options = options
        self.value = None
        self.ref = None
        if self.type == "random_choices":
            if not self.options.elements or len(self.options.elements) < 2:
                raise ValueError(f"random_choices generator requires that you pass `elements` list in options")
        try:
            fake = Faker()
            if self.type != "random_choices":
                getattr(fake, self.type)()
        except AttributeError:
            raise ValueError(f"{self.type} is not a fake generator")
        if self.options and self.options.elements:
            self.ref = {k+1: v for k, v in enumerate(self.options.elements)}
            self.ref_frame = pd.DataFrame([{"id": k, "value": v} for k,v in self.ref.items()])


class ColumnCollection:

    def __init__(self):
        self.__collection__ = list()

    def add(self, col: Column):
        if len(self.__collection__) > 0 and any([x for x in self.__collection__ if x.name == col.name]):
            raise ValueError(f"Column with name {col.name} already exists, use replace()")
        self.__collection__.append(col)

    def remove(self, col_name: str):
        new_col = [x for x in self.__collection__ if x.name != col_name]

    def list_names(self):
        return [x.name for x in self.__collection__]

    def __iter__(self):
        return iter(self.__collection__)


class Config:

    def __init__(self, name: str,
                 num_rows: int,
                 locale: str = "en_US",
                 path: str = "./",
                 delimiter: str = ",",
                 format: str = "csv",
                 terminator: str = os.linesep):
        self.num_rows = num_rows
        self.locale = locale
        self.name = name
        self.path = path
        self.delimiter = delimiter
        self.format = format


class Feed:

    def __init__(self, config: Config):
        self.config = config
        self.columns = ColumnCollection()
        self.data = list()
        self.data_frame: pd.DataFrame = None

    @classmethod
    def from_spec(cls, file: str):
        start = datetime.now()
        logging.info(f"Trying to generate from {file} spec")
        spec = json.loads(open(file, "r").read())
        config = Config(**spec.get("config"))
        feed = Feed(config)
        for col in spec.get("columns"):
            if col.get("options"):
                opts = Options(**col.get("options"))
                col.pop("options")
                feed.columns.add(Column(**col, options=opts))
            else:
                feed.columns.add(Column(**col))
        feed.generate()
        feed.to_csv2()
        print(f"Done generating feed in {config.path}, {config.num_rows} took {(datetime.now()-start).total_seconds()} seconds")

    def generate(self) -> pd.DataFrame:
        fake = Faker(locale=self.config.locale)
        Faker.seed(0)
        self.data = list()
        for r in range(self.config.num_rows):
            row = dict()
            for c in self.columns:
                if c.type == "random_choices":
                    if c.options.ref_dict:
                        row[c.name] = getattr(fake, c.type)(c.ref.keys(), 1)[0]
                    else:
                        row[c.name] = getattr(fake, c.type)(c.ref.values(), 1)[0]
                elif c.type == "address":
                    row[c.name] = fake.address().replace(os.linesep, ", ")
                else:
                    row[c.name] = getattr(fake, c.type)()
            self.data.append(row)
            self.data_frame = pd.DataFrame(self.data)
        return self.data_frame

    def to_csv2(self, path: str = None, delimiter: str = None, terminator: str = os.linesep):
        if not path:
            path = self.config.path
        if not delimiter:
            delimiter = self.config.delimiter
        name = os.path.join(path, f"{self.config.name}_base.csv")
        self.data_frame.to_csv(name, sep=delimiter, index=False, lineterminator=terminator)
        for column in self.columns:
            if column.type == "random_choices":
                self.data_frame[f"{column.name}_id"] = \
                    self.data_frame[column.name].map(column.ref_frame.set_index("value")["id"])
        name = os.path.join(path, f"{self.config.name}_all.csv")
        self.data_frame.to_csv(name, sep=delimiter, index=False, lineterminator=terminator)
        to_drop = [x.name for x in self.columns if x.type == "random_choices"]
        name = os.path.join(path, f"{self.config.name}_refs.csv")
        self.data_frame.drop(to_drop, axis=1).to_csv(name, sep=delimiter, index=False,
                                                     lineterminator=terminator)
        for col in self.columns:
            if col.name in to_drop:
                name = os.path.join(path, f"{self.config.name}_{col.name}.csv")
                col.ref_frame.to_csv(name, sep=delimiter, index=False, lineterminator=terminator)

    def to_csv(self, path: str, delimiter: str):
        # export the feed
        filename = os.path.join(path, self.config.name+".csv")
        with open(filename, "w", newline='') as file:
            writer = csv.DictWriter(file, delimiter=delimiter, fieldnames=self.data[0].keys())
            writer.writeheader()
            for i in self.data:
                writer.writerow(i)
        # export all ref columns
        for c in self.columns:
            if c.options and c.options.ref_dict:
                filename = os.path.join(path, self.config.name + "_" + c.name.replace(" ", "") + ".csv")
                with open(filename, "w", newline="") as file:
                    writer = csv.DictWriter(file, delimiter=delimiter, fieldnames=["id", "value"])
                    writer.writeheader()
                    for i in c.ref:
                        writer.writerow({"id": i, "value": c.ref[i]})


if __name__ == "__main__":
    if len(sys.argv) != 2:
        print("Need path to spec file and only that: `python feed_maker spec.json`")
    else:
        Feed.from_spec(sys.argv[1])

