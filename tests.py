from unittest import TestCase
from feed_maker import Feed, Config, Column, Options
import os


class Basics(TestCase):

    def test_basic_construction(self):
        num_rows = 1000
        path_csv = "./"
        feed_config = Config(locale="en_US", num_rows=num_rows, name="test_feed")
        feed = Feed(config=feed_config)
        feed.columns.add(Column(name="Name", column_type="name"))
        feed.columns.add(Column(name="E-mail", column_type="email"))
        feed.columns.add(Column(name="Gender", column_type="random_choices",
                                options=Options(elements=["Male", "Female"], ref_dict=True)))
        data = feed.generate()
        self.assertEqual(len(feed.data), num_rows)
        feed.to_csv(path_csv, ",")
        feed.to_csv2(path_csv, "|")
        self.assertTrue(os.path.exists(path_csv))
        self.assertEqual(data.shape, (num_rows, 4))


